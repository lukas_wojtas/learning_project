package pl.spring_learning.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.properties.ProductWriterProperties;
import pl.spring_learning.services.IKafkaService;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import java.net.URI;
import java.util.UUID;

/**
 * @Author Lukasz Wojtas
 */
@Slf4j
@RestController
public class ProductWriterRestController {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private IKafkaService kafkaService;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private DataValidator validator;

    private final int port;

    private final String host;

    @Autowired
    public ProductWriterRestController(ProductWriterProperties props) {
        this.host = props.getHost();
        this.port = props.getPort();
    }

    @Operation(description = "add product to stock", tags = {" manage products "})
    @ApiResponse(responseCode = "303", description = "see other",
            headers = @Header(name = "location",
                    description = "provide url to track transaction",
                    required = true,
                    schema = @Schema(type = "string")
            ))
    @ApiResponse(responseCode = "400", description = "Bad request",
            content = @Content(schema = @Schema(implementation = RestControllerMessage.class)))
    @RequestMapping(value = "/admin/createProduct", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public ResponseEntity<Void> createProduct(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "ClientRequest body.",
                    content = @Content(schema = @Schema(implementation = ProductEntity.class)), required = true)
            @RequestBody ProductEntity product) throws RestControllerException {
        try {
            String productEntity = this.mapper.writerFor(ProductEntity.class).writeValueAsString(product);
            this.validator.validate(productEntity);
            KafkaTransactionMessage message =
                    this.getKafkaTransactionMessage(KafkaOperations.CREATE_PRODUCT, productEntity);
            this.transactionService.addTransaction(message);
            this.kafkaService.sendMessage(message, KafkaTopics.CREATE_PRODUCT);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(getUri(message.getTransactionId()));
            return new ResponseEntity<>(headers, HttpStatus.SEE_OTHER);
        } catch (JsonProcessingException | ValidationException e) {
            log.error(e.getLocalizedMessage());
            throw new RestControllerException("Processing request failed");
        }
    }

    @Operation(description = "remove product from stock", tags = {" manage products "})
    @ApiResponse(responseCode = "303", description = "see other",
            headers = @Header(name = "location",
                    description = "provide url to track transaction",
                    required = true,
                    schema = @Schema(type = "string")
            ))
    @ApiResponse(responseCode = "400", description = "Bad request",
            content = @Content(schema = @Schema(implementation = RestControllerMessage.class)))
    @RequestMapping(value = "/admin/deleteProduct", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public ResponseEntity<Void> deleteProduct(@RequestParam(name="name") String productId) throws RestControllerException {
        try {
            ProductEntity product = new ProductEntity();
            product.setName(productId);
            product.setPrice(0);
            String productEntity = this.mapper.writerFor(ProductEntity.class).writeValueAsString(product);
            KafkaTransactionMessage message =
                    this.getKafkaTransactionMessage(KafkaOperations.DELETE_PRODUCT, productEntity);
            this.transactionService.addTransaction(message);
            this.kafkaService.sendMessage(message, KafkaTopics.DELETE_PRODUCT);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(getUri(message.getTransactionId()));
            return new ResponseEntity<>(headers, HttpStatus.SEE_OTHER);
        } catch (JsonProcessingException e) {
            throw new RestControllerException("Processing request failed");
        }
    }

    @ExceptionHandler({RestControllerException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestControllerMessage handleRequestError(RestControllerException e) {
        return e.getRestControllerMessage();
    }

    private URI getUri(String id) {
        return UriComponentsBuilder
                .newInstance()
                .scheme("https")
                .port(this.port)
                .host(this.host)
                .path(OperationResultRestController.PATH)
                .queryParam("id", id).build().toUri();
    }

    private KafkaTransactionMessage getKafkaTransactionMessage(KafkaOperations operation, String payload) {
        String id = UUID.randomUUID().toString();
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setTransactionId(id);
        message.setPayload(payload);
        message.setOperation(operation);
        message.setResult(KafkaTransactionResult.IN_PROGRESS);
        return message;
    }
}
