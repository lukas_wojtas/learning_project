package pl.spring_learning.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.properties.TransactionCacheProperties;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.validator.DataValidator;

import java.util.Map;
import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 */
@Service
public class TransactionService implements ITransactionService {

    private final int transactionTtl;

    @Autowired
    public TransactionService(TransactionCacheProperties props) {
        this.transactionTtl = props.getTtl();
    }

    @Autowired
    private Map<String, TransactionDescription> transactionCache;

    @Autowired
    private DataValidator validator;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public void addTransaction(KafkaTransactionMessage message) {
        Optional.ofNullable(this.transactionCache.get(message.getTransactionId()))
                .ifPresentOrElse(transaction -> {
                    transaction.setResult(message.getResult());
                }, () -> transactionCache.put(message.getTransactionId(),
                        new TransactionDescription(
                                System.currentTimeMillis(),
                                message.getOperation(),
                                message.getResult(),
                                message.getTransactionId())));
    }

    @Override
    public Optional<TransactionDescription> getTransaction(String id) {
        return Optional.ofNullable(this.transactionCache.get(id));
    }

    @Scheduled(fixedRateString = "${cache.refresh}")
    private void setUpCache() {
        long now = System.currentTimeMillis();
        this.transactionCache
                .values()
                .stream()
                .filter(transaction -> now - (1000 * transactionTtl) > transaction.getTimestamp())
                .forEach(transaction -> {
                    if (transaction.getResult().equals(KafkaTransactionResult.IN_PROGRESS)) {
                        transaction.setResult(KafkaTransactionResult.FAILED);
                        transaction.setTimestamp(now);
                    } else {
                        this.transactionCache.remove(transaction.getTransactionId());
                    }
                });
    }
}
