package pl.spring_learning.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 */

@Slf4j
@Service
public class KafkaService implements IKafkaService {

    private static final String WRITER_GROUP_ID = "writer_group_id";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private DataValidator validator;

    @Override
    public void sendMessage(KafkaTransactionMessage kafkaMessage, String topic) {
        try {
            String message = this.mapper.writeValueAsString(kafkaMessage);
            kafkaTemplate.send(topic, message)
                    .completable()
                    .whenComplete((success, fail) -> Optional.ofNullable(fail).ifPresentOrElse(f -> {
                        log.error("Sending to kafka failed with : " + f.getLocalizedMessage());
                        kafkaMessage.setResult(KafkaTransactionResult.FAILED);
                        this.transactionService.addTransaction(kafkaMessage);
                    }, () -> this.transactionService.addTransaction(kafkaMessage)));
        } catch (JsonProcessingException e) {
            log.error("Mapping Kafka Transaction message failed with: " + e.getMessage());

        }
    }

    @KafkaListener(topics = KafkaTopics.PRODUCT_RESULT, groupId = WRITER_GROUP_ID)
    public void getTransactionResult(@Payload String message) {
        try {
            KafkaTransactionMessage msg = this.mapper
                    .readerFor(KafkaTransactionMessage.class).readValue(message);
            this.validator.validate(message);
            this.transactionService.getTransaction(msg.getTransactionId())
                    .ifPresent(transaction -> transaction.setResult(msg.getResult()));
        } catch (JsonProcessingException | ValidationException e) {
            log.error("Kafka message unknown " + e.getLocalizedMessage());
        }
    }
}
