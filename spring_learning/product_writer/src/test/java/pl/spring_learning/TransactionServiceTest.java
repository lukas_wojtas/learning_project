package pl.spring_learning;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.services.TransactionService;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.utils.TransactionServiceContextConfiguration;

import java.util.Map;
import java.util.Optional;

/**
 * @Author Ewa SKiba
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {
        TransactionServiceContextConfiguration.class,
        TransactionService.class})
@EnableAutoConfiguration
public class TransactionServiceTest {

    @Autowired
    private Map<String, TransactionDescription> transactionCache;

    @Autowired
    TransactionService transactionService;

    @Before
    public void setUp() {
        transactionCache.clear();
    }

    @Test
    public void testAddNewTransaction() {
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setPayload("payload");
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setTransactionId("transaction1");
        this.transactionService.addTransaction(message);
        Assert.assertEquals(this.transactionCache.get("transaction1").getResult(), message.getResult());
    }

    @Test
    public void testAddTransactionWithExistingTransactionID() {
        TransactionDescription transaction = new TransactionDescription(System.currentTimeMillis(), KafkaOperations.CREATE_PRODUCT, KafkaTransactionResult.IN_PROGRESS, "transaction1");
        this.transactionCache.put("transaction1", transaction);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setPayload("payload");
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setTransactionId("transaction1");
        this.transactionService.addTransaction(message);
        Assert.assertEquals(transaction.getResult(), message.getResult());
    }

    @Test
    public void testGetTransaction() {
        TransactionDescription transaction = new TransactionDescription(System.currentTimeMillis(), KafkaOperations.CREATE_PRODUCT, KafkaTransactionResult.SUCCESS, "transaction1");
        this.transactionCache.put("transaction1", transaction);
        Optional<TransactionDescription> receivedTransaction = this.transactionService.getTransaction("transaction1");
        receivedTransaction.ifPresentOrElse(p -> {
            Assert.assertEquals(receivedTransaction.get().getOperation(), transaction.getOperation());
            Assert.assertEquals(receivedTransaction.get().getResult(), transaction.getResult());
        }, Assert::fail);
    }
}
