package pl.spring_learning.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import pl.spring_learning.services.IKafkaService;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;

public class ProductWriterControllersBeanConfiguration {

    @Bean
    public IKafkaService getKafkaService() {
        return Mockito.mock(IKafkaService.class);
    }

    @Bean
    public ITransactionService getTransactionService() {
        return Mockito.mock(ITransactionService.class);
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public DataValidator getDataValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

}
