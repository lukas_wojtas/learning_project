package pl.spring_learning.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.services.IKafkaService;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.services.KafkaService;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;
import java.util.Map;

import static pl.spring_learning.utils.KafkaTestListener.TEST_GROUP_ID;

/**
 * @Author Lukasz Wojtas
 * */
@Configuration
public class KafkaServiceContextConfiguration {

    @Bean
    public ITransactionService getTransactionService() {
        return Mockito.mock(ITransactionService.class);
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public DataValidator getDataValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

    @Bean
    public ProducerFactory<String, String> producerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        return new DefaultKafkaProducerFactory<>(KafkaTestUtils.producerProps(kafkaEmbedded));
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate(EmbeddedKafkaBroker kafkaBroker) {
        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<>(producerFactory(kafkaBroker));
        kafkaTemplate.setDefaultTopic(KafkaTopics.PRODUCT_RESULT);
        return kafkaTemplate;
    }

    @Bean
    public IKafkaService getKafkaService() {
        return new KafkaService();
    }

    @Bean
    public ConsumerFactory<String, String> consumerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        Map<String, Object> props = KafkaTestUtils.consumerProps(TEST_GROUP_ID, "false", kafkaEmbedded);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(kafkaEmbedded));
        return factory;
    }
}
