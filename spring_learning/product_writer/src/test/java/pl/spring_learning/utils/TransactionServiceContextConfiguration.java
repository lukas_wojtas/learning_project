package pl.spring_learning.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import pl.spring_learning.properties.TransactionCacheProperties;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.services.TransactionService;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Ewa Skiba
 * */
public class TransactionServiceContextConfiguration {

    @Bean
    public Map<String, TransactionDescription> getTransactionCache(){
        return new ConcurrentHashMap<>();
    }

    @Bean
    public TransactionCacheProperties getTransactionCacheProps() {
        TransactionCacheProperties props = new TransactionCacheProperties();
        props.setRefresh("1000");
        props.setTtl(1);
        return props;
    }

    @Bean
    public ITransactionService getTransactionService() {
        return new TransactionService(getTransactionCacheProps());}

    @Bean
    public DataValidator getDataValidator(){
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

    @Bean
    public ObjectMapper getObjectMapper(){return new ObjectMapper();}
}
