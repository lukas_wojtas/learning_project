package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.testng.Assert;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.controllers.OperationResultRestController;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.utils.ProductWriterControllersBeanConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ProductWriterControllersBeanConfiguration.class,
        OperationResultRestController.class,
        ServiceSecurityConfiguration.class
})
@AutoConfigureMockMvc
@EnableWebMvc
public class OperationResultRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private OperationResultRestController operationResultRestController;

    @Test
    public void testGetTransactionStatus() {
        Mockito.when(this.transactionService.getTransaction(any())).thenReturn(Optional.of(new TransactionDescription(System.currentTimeMillis(), KafkaOperations.DELETE_PRODUCT, KafkaTransactionResult.SUCCESS, "123-123")));
        TransactionDescription transactionDescription = operationResultRestController.getTransactionStatus("213");
        Assert.assertEquals(transactionDescription.getOperation(), KafkaOperations.DELETE_PRODUCT);
        Assert.assertEquals(transactionDescription.getResult(), KafkaTransactionResult.SUCCESS);
        Assert.assertEquals(transactionDescription.getTransactionId(), "123-123");
    }

    @Test
    public void testGetTransactionStatusWhenReturnEmptyDescription(){
        Mockito.when(this.transactionService.getTransaction(any())).thenReturn(Optional.empty());
        RestControllerException exception = assertThrows(RestControllerException.class, () -> {
            operationResultRestController.getTransactionStatus("213");
        });
        Assert.assertEquals("Transaction not found", exception.getRestControllerMessage().getMessage());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testExceptionHandlingForGetTransactionStatus() throws Exception{
        Mockito.when(this.transactionService.getTransaction(any())).thenReturn(Optional.empty());
        mockMvc.perform(MockMvcRequestBuilders.get("/transactionstatus")
                .param("id", "5a5")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(new RestControllerMessage("Transaction not found"))));
    }
}
