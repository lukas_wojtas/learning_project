package pl.spring_learning.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author Lukasz Wojtas
 * */

@Configuration
public class CommonContext {

    @Bean
    ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    Map<String, ProductEntity> getProductCache() {
        return new HashMap<>();
    }

    @Bean
    DataValidator getValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

}
