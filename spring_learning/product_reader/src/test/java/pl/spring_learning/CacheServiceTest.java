package pl.spring_learning;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.exceptions.base.MockitoException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.junit4.SpringRunner;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.services.CacheService;
import pl.spring_learning.services.ICacheService;
import pl.spring_learning.utils.CacheServiceContextConfiguration;
import pl.spring_learning.utils.CommonContext;
import pl.spring_learning.utils.TestDataLoader;
import pl.spring_learning.validator.DataValidator;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

/**
 * @Author Lukasz Wojtas
 * */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        CacheService.class,
        CacheServiceContextConfiguration.class,
        CommonContext.class
})
@AutoConfigureMockMvc
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:3333",
                "port=3333"
        })
@EnableAutoConfiguration
public class CacheServiceTest {

    @Mock
    private Appender<ILoggingEvent> mockAppender;

    @Captor
    private ArgumentCaptor<ILoggingEvent> arg;

    @Autowired
    private Map<String, ProductEntity> cache;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private KafkaTemplate<String, String> template;

    @Autowired
    private TestDataLoader testDataLoader;

    @Autowired
    private IProductDaoReader productDaoReader;

    @Autowired
    private DataValidator validator;

    @Autowired
    private ICacheService cacheService;

    @Before
    public void setUp() {
        when(this.productDaoReader
                .getAllProducts()).thenReturn(testDataLoader.getProducts());
        Logger logger = (Logger) LoggerFactory.getLogger(CacheService.class.getName());
        logger.addAppender(mockAppender);
        this.cache.remove("name");
        this.cache.remove("extra");
    }

    @Test
    public void testHandlingKafkaMessageWithoutOperation() throws JsonProcessingException {
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setTransactionId("transactionId");
        message.setPayload("payload");
        this.template.send(KafkaTopics.PRODUCT_RESULT,
                mapper.writer().writeValueAsString(message))
                .completable().whenComplete((success, fail) -> {
            if (fail != null) {
                Assert.fail();
            }
        });
        this.template.flush();
        await()
                .pollInterval(1, TimeUnit.SECONDS)
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(mockAppender, atMost(1)).doAppend(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) {
                            return false;
                        } else {
                            return arg.getValue()
                                    .getMessage().contains("Failed to handle message with exception:");
                        }
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testHandlingMessageWithWrongPayload() throws JsonProcessingException {
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setPayload("bbbrrrrr");
        this.template.send(KafkaTopics.PRODUCT_RESULT,
                mapper.writer().writeValueAsString(message))
                .completable().whenComplete((success, fail) -> {
            if (fail != null) {
                Assert.fail();
            }
        });
        this.template.flush();
        await()
                .pollInterval(1, TimeUnit.SECONDS)
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(mockAppender, atMost(1)).doAppend(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) {
                            return false;
                        } else {
                            return arg.getValue()
                                    .getMessage().contains("Failed to handle message with exception: Unrecognized token 'bbbrrrrr'");
                        }
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testHandlingKafkaMessageWithIncorrectProduct() throws JsonProcessingException {
        ProductEntity entity = new ProductEntity();
        String productJson = this.mapper.writer().writeValueAsString(entity);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setPayload(productJson);
        this.template.send(KafkaTopics.PRODUCT_RESULT,
                this.mapper.writeValueAsString(message))
                .completable().whenComplete((success, fail) -> {
            if (fail != null) {
                Assert.fail();
            }
        });
        this.template.flush();
        await()
                .pollInterval(1, TimeUnit.SECONDS)
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(mockAppender, atMost(1)).doAppend(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) {
                            return false;
                        } else {
                            return arg.getValue()
                                    .getMessage().contains("Failed to handle message with exception:");
                        }
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testHandlingIncorrectKafkaMessage() {
        this.template.send(KafkaTopics.PRODUCT_RESULT, "message")
                .completable().whenComplete((success, fail) -> {
            if (fail != null) {
                Assert.fail();
            }
        });
        this.template.flush();
        await()
                .pollInterval(1, TimeUnit.SECONDS)
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(mockAppender, atMost(1)).doAppend(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) {
                            return false;
                        } else {
                            return arg.getValue()
                                    .getMessage().contains("Failed to handle message with exception: Unrecognized token 'message'");
                        }
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testAddAndRemoveNewProductFromKafka() throws JsonProcessingException {
        ProductEntity entity = new ProductEntity();
        entity.setName("name");
        entity.setPrice(10);
        String productJson = this.mapper.writer().writeValueAsString(entity);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setPayload(productJson);
        this.template.send(KafkaTopics.PRODUCT_RESULT,
                this.mapper.writeValueAsString(message))
                .completable()
                .whenComplete((success, fail) ->
                        Optional.of(fail).ifPresent(f -> Assert.fail()));
        this.template.flush();
        await()
                .atMost(20, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .until(() -> this.cache.get("name") != null);
        ProductEntity p = this.cache.get("name");
        Assert.assertEquals(10, p.getPrice().intValue());
        Assert.assertEquals(0, p.getSpecialPrice().intValue());
        Assert.assertEquals(0, p.getAmount().intValue());
        message.setOperation(KafkaOperations.DELETE_PRODUCT);
        this.template.send(KafkaTopics.PRODUCT_RESULT, this.mapper.writeValueAsString(message))
                .completable()
                .whenComplete((success, fail) ->
                    Optional.of(fail).ifPresent(f -> Assert.fail()));
        this.template.flush();
        await()
                .atMost(10, TimeUnit.SECONDS)
                .pollInterval(1, TimeUnit.SECONDS)
                .until(() -> this.cache.get("name") == null);
    }

    @Test
    public void testUnknownOperationInMessage() throws JsonProcessingException {
        ProductEntity entity = new ProductEntity();
        entity.setName("name");
        entity.setPrice(10);
        entity.setAmount(1);
        String productJson = this.mapper.writer().writeValueAsString(entity);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setPayload(productJson);
        String rawJson = this.mapper.writeValueAsString(message);
        rawJson = rawJson.replace("CREATE_PRODUCT", "FAKE");
        this.template.send(KafkaTopics.PRODUCT_RESULT, rawJson)
                .completable()
                .whenComplete((success, fail) ->
                        Optional.of(fail).ifPresent(f -> Assert.fail()));
        this.template.flush();
        await()
                .pollInterval(1, TimeUnit.SECONDS)
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(mockAppender, atMost(1)).doAppend(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) {
                            return false;
                        } else {
                            return arg.getValue()
                                    .getMessage().contains("Failed to handle message with exception: " +
                                            "Cannot deserialize value of type `pl.spring_learning.definitions.KafkaOperations` " +
                                            "from String \"FAKE\"");
                        }
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testProductNotInCacheButPresentInStock() {
        final String extra = "extra";
        ProductEntity entity = new ProductEntity();
        entity.setName(extra);
        entity.setPrice(10);
        when(this.productDaoReader.getProduct(extra)).thenReturn(Optional.of(entity));
        ProductEntity p = this.cacheService.getProductByName(extra);
        Assert.assertEquals(entity, p);
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
        verify(this.productDaoReader, times(1)).getProduct(argument.capture());
        Assert.assertEquals(extra, argument.getValue());
    }

    @Test
    public void testCacheEmptyAtStart() {
        this.cache.clear();
        reset(this.productDaoReader);
        cacheService.getProducts();
        verify(productDaoReader, times(1)).getAllProducts();
    }
}
