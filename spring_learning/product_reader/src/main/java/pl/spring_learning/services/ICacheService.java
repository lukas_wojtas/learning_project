package pl.spring_learning.services;

import pl.spring_learning.entity.ProductEntity;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */

public interface ICacheService {

    List<ProductEntity> getProducts();

    ProductEntity getProductByName(String name);

}
