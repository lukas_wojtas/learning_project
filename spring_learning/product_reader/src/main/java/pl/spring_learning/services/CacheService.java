package pl.spring_learning.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * @Author Lukasz Wojtas
 */

@Slf4j
@Service
public class CacheService implements ICacheService {

    public static final String PRODUCT_READER_CONSUMER_ID = "Product_reader_consumer_id";
    public static final String NO_PRODUCT_IN_STOCK = "No Product in stock";

    private final Map<String, ProductEntity> products;

    private final DataValidator validator;

    private final IProductDaoReader daoReader;

    private final ObjectMapper mapper;

    @Autowired
    public CacheService(
            Map<String, ProductEntity> products,
            DataValidator validator,
            IProductDaoReader daoReader,
            ObjectMapper mapper) {
        this.products = products;
        this.validator = validator;
        this.daoReader = daoReader;
        this.mapper = mapper;
        this.initializeCache();
    }

    @Override
    public List<ProductEntity> getProducts() {
        if (this.products.isEmpty()) {
            this.initializeCache();
        }
        return new ArrayList<>(this.products.values());
    }

    @Override
    public ProductEntity getProductByName(String name) {
        if (!this.products.containsKey(name)) {
            this.products.put(name, this.daoReader.getProduct(name)
                    .orElseThrow(() -> new RestControllerException(NO_PRODUCT_IN_STOCK)));
        }
        return this.products.get(name);
    }

    @KafkaListener(topics = KafkaTopics.PRODUCT_RESULT, id = PRODUCT_READER_CONSUMER_ID)
    private void receiveKafkaAnnotations(String message) {
        try {
            KafkaTransactionMessage msg = this.mapper.reader()
                    .forType(KafkaTransactionMessage.class).readValue(message);
            this.validator.validate(msg);
            ProductEntity p = this.mapper.reader()
                    .forType(ProductEntity.class).readValue(msg.getPayload());
            this.validator.validate(p);
            switch (msg.getOperation()) {
                case CREATE_PRODUCT:
                    this.performUpdateCache(msg.getResult(), p,
                            product -> this.products.put(p.getName(), p));
                    break;
                case DELETE_PRODUCT:
                    this.performUpdateCache(msg.getResult(), p,
                            product -> this.products.remove(product.getName()));
                    break;
            }
        } catch (JsonProcessingException | RestControllerException | ValidationException e) {
            log.error("Failed to handle message with exception: "
                    + e.getLocalizedMessage());
        }
    }

    private void performUpdateCache(KafkaTransactionResult result, ProductEntity p, Consumer<ProductEntity> consumer) {
        Optional.ofNullable(result)
                .filter(r -> r == KafkaTransactionResult.SUCCESS)
                .ifPresent(r -> consumer.accept(p));
    }

    private void initializeCache() {
        daoReader.getAllProducts().forEach(productEntity -> {
            if (!this.products.containsKey(productEntity.getName())) {
                this.products.put(productEntity.getName(), productEntity);
            }
        });
    }
}
