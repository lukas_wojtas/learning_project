package pl.spring_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Lukasz Wojtas
 * */

@SpringBootApplication
@EnableAutoConfiguration
public class ProductReaderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductReaderServiceApplication.class, args);
    }
}
