package pl.spring_learning.core.dao_abstraction;

import pl.spring_learning.entity.PersonEntity;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */

public interface IPersonDaoReader {
    List<PersonEntity> getPeople();
}
