package pl.spring_learning.core.dao_abstraction;

import pl.spring_learning.entity.ProductEntity;

import java.util.List;
/**
 * @Author Lukasz Wojtas
 * */

public interface IProductDaoWriter {

    void addProduct(ProductEntity product);

    void addProducts(List<ProductEntity> products);

    void removeProduct(ProductEntity product);

    void removeProducts(List<ProductEntity> products);

    void updateProduct(ProductEntity product);

    void removeProductByName(String name);

}
