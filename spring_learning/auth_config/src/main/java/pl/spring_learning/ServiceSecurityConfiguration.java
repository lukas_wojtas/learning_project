package pl.spring_learning;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.HttpBasicConfigurer;
import pl.spring_learning.model.UrlConfig;
import pl.spring_learning.model.UserConfig;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "auth")
public class ServiceSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private List<UserConfig> users;
    private List<UrlConfig> urls;

    private static final String ROLE_USER = "USER";

    private static final String ROLE_ADMIN = "ADMIN";

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig =
                auth.inMemoryAuthentication();
        this.users.forEach(user -> userConfig.withUser(user.getUser())
                .password("{noop}" + user.getPassword())
                .roles(user.getRoles().toArray(new String[0]))
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        HttpBasicConfigurer<HttpSecurity> basicConfig = http.httpBasic();
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry =
                basicConfig.and().authorizeRequests();
        urls.forEach(urlConfig -> urlRegistry.antMatchers(
                this.getMethodFromConfig(urlConfig.getHttpMethod()),
                urlConfig.getUrls().toArray(new String[0]))
                .hasRole(urlConfig.getRole()));
        urlRegistry
                .and()
                .csrf()
                .disable()
                .formLogin()
                .disable();
    }

    private HttpMethod getMethodFromConfig(String method) {
        switch (method) {
            case "post":
                return HttpMethod.POST;
            case "put":
                return HttpMethod.PUT;
            case "delete":
                return HttpMethod.DELETE;
            case "get":
                return HttpMethod.GET;
            default:
                throw new RuntimeException("Method not recognized");
        }
    }
}
