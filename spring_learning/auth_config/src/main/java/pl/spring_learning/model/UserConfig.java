package pl.spring_learning.model;

import lombok.Data;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */
@Data
public class UserConfig {
    private String user;
    private String password;
    private List<String> roles;
}
