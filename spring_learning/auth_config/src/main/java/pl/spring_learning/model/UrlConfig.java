package pl.spring_learning.model;

import lombok.Data;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */
@Data
public class UrlConfig {
    private List<String> urls;
    private String role;
    private String httpMethod;
}
