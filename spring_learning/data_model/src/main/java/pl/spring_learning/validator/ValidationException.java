package pl.spring_learning.validator;

/**
 * @Author Lukasz Wojtas
 * */
public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
