package pl.spring_learning.validator;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author Lukasz Wojtas
 * */

public class DataValidator {

    private final Validator validator;

    public DataValidator(ValidatorFactory validatorFactory) {
        validator = validatorFactory.getValidator();
    }

    public <T> boolean validate(T clazz) throws ValidationException {
        Set<ConstraintViolation<T>> violations
                = validator.validate(clazz);
        if(!violations.isEmpty()) {
            throw new ValidationException(new ArrayList<>(violations).toString());
        }
        return true;
    }
}
