package pl.spring_learning.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name="person")
public class PersonEntity {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Schema(description = "identifier of person")
    private Integer id;

    @Column(name = "name")
    @Schema(description = "person name")
    private String name;
}
