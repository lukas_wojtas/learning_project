package pl.spring_learning.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author Lukasz Wojtas
 * */
@Data
@EqualsAndHashCode
@Entity
@Table(name = "products")
public class ProductEntity {

    public ProductEntity() {
        this.amount = 0;
        this.specialPrice = 0;
    }

    @Id
    @Column(name = "name")
    @NotNull
    @NotEmpty
    @Schema(description = "Product name")
    private String name;

    @Column(name = "price")
    @NotNull
    @Schema(description = "product price")
    private Integer price;

    @Column(name = "specialPrice")
    @Schema(description = "product special price")
    private Integer specialPrice;

    @Column(name = "amount")
    @Schema(description = "for how much product special price is on offer")
    private Integer amount;
}
