package pl.spring_learning.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 */


@Data
@AllArgsConstructor
public class AllProducts {

    @Schema(description = "list of all products in stock")
    private List<ProductEntity> products;

}
