package pl.spring_learning;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import pl.spring_learning.entity.PersonEntity;
import pl.spring_learning.persistence.dao.person.PersonDaoReader;
import pl.spring_learning.persistence.dao.person.PersonDaoWriter;
import pl.spring_learning.persistence.repository.PersonRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @Author Lukasz Wojtas
 */

public class TestPersonDaoOnMock {
    private final PersonRepository repository = Mockito.mock(PersonRepository.class);
    private PersonDaoWriter personDaoWriter;
    private PersonDaoReader personDaoReader;
    private PersonEntity personEntity;
    private static final String NO_CONNECTION_TO_DATABASE = "No connection to database";

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        Mockito.reset(repository);
        personDaoWriter = new PersonDaoWriter(repository);
        personDaoReader = new PersonDaoReader(repository);
        personEntity = new PersonEntity();
    }

    @Test
    public void testSavePerson() {
        personDaoWriter.savePerson(personEntity);
        verify(repository, times(1)).save(personEntity);
    }

    @Test
    public void testSavePersonWithException() {
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository).save(personEntity);
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        personDaoWriter.savePerson(personEntity);
    }

    @Test
    public void testGetPeople() {
        List<PersonEntity> list = Collections.singletonList(personEntity);
        when(repository.findAll()).thenReturn(list);
        Assert.assertEquals(list, personDaoReader.getPeople());
    }

    @Test
    public void testGetAllPeopleWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        when(repository.findAll())
                .thenThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE));
        personDaoReader.getPeople();
    }

    @Test
    public void testRemovePerson() {
        personDaoWriter.removePerson(personEntity);
        verify(repository, times(1)).delete(personEntity);
    }

    @Test
    public void testRemovePersonWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .delete(personEntity);
        personDaoWriter.removePerson(personEntity);
    }

    @Test
    public void testGetPerson() {
        final int id = 0;
        when(repository.findById(id)).thenReturn(Optional.of(personEntity));
        personDaoWriter.getPerson(id).ifPresent(p -> Assert.assertEquals(personEntity, p));
    }

    @Test
    public void testGetPersonWithException() {
        final int id = 0;
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        when(repository.findById(id))
                .thenThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE));
        personDaoWriter.getPerson(id);
    }

    @Test
    public void testRemovePeople() {
        List<PersonEntity> list = Collections.singletonList(personEntity);
        personDaoWriter.removePeople(list);
        verify(repository, times(1)).deleteAll(list);
    }

    @Test
    public void testRemovePeopleWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        List<PersonEntity> list = Collections.singletonList(personEntity);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository).deleteAll(list);
        personDaoWriter.removePeople(list);
    }
}
