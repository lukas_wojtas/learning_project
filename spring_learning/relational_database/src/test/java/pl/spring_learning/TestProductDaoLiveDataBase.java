package pl.spring_learning;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.core.dao_abstraction.IProductDaoWriter;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.persistence.PersistenceConfig;
/**
 * @Author Lukasz Wojtas
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceConfig.class)
@SpringBootTest
@Ignore
public class TestProductDaoLiveDataBase extends AbstractTestNGSpringContextTests {

    @Autowired
    private IProductDaoReader productDaoReader;

    @Autowired
    private IProductDaoWriter productDaoWriter;

    private static final String PRODUCT_NAME = "Very best skies";

    @BeforeMethod(groups = "LiveDataSource")
    public void setUp() {
        if (this.productDaoReader.checkIfProductInStock(PRODUCT_NAME)) {
            this.productDaoWriter.removeProductByName(PRODUCT_NAME);
        }
    }

    @Test(groups = {"LiveDataSource"})
    public void testAddRemoveProduct() {
        ProductEntity product = new ProductEntity();
        product.setName(PRODUCT_NAME);
        product.setPrice(1234);
        this.productDaoWriter.addProduct(product);
        this.productDaoReader.getProduct(PRODUCT_NAME)
                .ifPresentOrElse(productEntity -> {
                    Assert.assertTrue(this.productDaoReader.checkIfProductInStock(PRODUCT_NAME));
                    Assert.assertEquals(product.getName(), productEntity.getName());
                    Assert.assertEquals(product.getPrice(), productEntity.getPrice());
                    this.productDaoWriter.removeProduct(product);
                }, Assert::fail);
    }


    @Test(groups = {"LiveDataSource"})
    public void testAddRemoveProductById() {
        ProductEntity product = new ProductEntity();
        product.setName(PRODUCT_NAME);
        product.setPrice(1234);
        this.productDaoWriter.addProduct(product);
        this.productDaoReader.getProduct(PRODUCT_NAME).ifPresentOrElse(p -> {
            Assert.assertTrue(this.productDaoReader.checkIfProductInStock(PRODUCT_NAME));
            Assert.assertNotNull(p);
            Assert.assertEquals(product.getName(), p.getName());
            Assert.assertEquals(product.getPrice(), p.getPrice());
            this.productDaoWriter.removeProductByName(PRODUCT_NAME);
            this.productDaoReader
                    .getProduct(PRODUCT_NAME)
                    .ifPresentOrElse(pp -> Assert.fail(), () -> {
                    });
        }, Assert::fail);
    }

    @Test(groups = {"LiveDataSource"}, expectedExceptions = DataAccessException.class)
    public void testTranslationOfExceptionsProvidedByRepositoryDecorator() {
        this.productDaoWriter.removeProductByName("strange name");
    }
}
