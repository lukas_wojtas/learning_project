package pl.spring_learning.persistence.dao.product;

import lombok.extern.slf4j.Slf4j;
import pl.spring_learning.persistence.repository.ProductRepository;
import pl.spring_learning.core.dao_abstraction.IProductDaoWriter;
import pl.spring_learning.entity.ProductEntity;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */

@Slf4j
public final class ProductDaoWriter implements IProductDaoWriter {

    private static final int MAX_NUMBER_OF_PRODUCTS_BEFORE_FLUSH = 100;

    private final ProductRepository repository;

    public ProductDaoWriter(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addProduct(ProductEntity product) {
        this.repository.save(product);
    }

    @Override
    public void addProducts(List<ProductEntity> products) {
        this.repository.saveAll(products);
    }

    @Override
    public void removeProduct(ProductEntity product) {
        this.repository.delete(product);
    }

    @Override
    public void removeProducts(List<ProductEntity> products) {
        this.repository.deleteAll(products);
    }

    @Override
    public void updateProduct(ProductEntity product) {
        this.repository.save(product);
    }

    @Override
    public void removeProductByName(String name) {
        this.repository.deleteById(name);
    }
}
