package pl.spring_learning.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import pl.spring_learning.entity.ProductEntity;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */
@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, String> {

    @NonNull
    @Override
    List<ProductEntity> findAll();
}
