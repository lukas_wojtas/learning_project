package pl.spring_learning.persistence.dao.person;

import lombok.extern.slf4j.Slf4j;
import pl.spring_learning.core.dao_abstraction.IPersonDaoReader;
import pl.spring_learning.entity.PersonEntity;
import pl.spring_learning.persistence.repository.PersonRepository;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */

@Slf4j
public final class PersonDaoReader implements IPersonDaoReader {

    private final PersonRepository repository;

    public PersonDaoReader(PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PersonEntity> getPeople() {
        return this.repository.findAll();
    }
}
