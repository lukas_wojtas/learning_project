/*
 * @author Lukasz Wojtas
 * To private use only.
 */

package pl.spring_learning.persistence.dao.person;

import lombok.extern.slf4j.Slf4j;
import pl.spring_learning.core.dao_abstraction.IPersonDaoWriter;
import pl.spring_learning.entity.PersonEntity;
import pl.spring_learning.persistence.repository.PersonRepository;

import java.util.List;
import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 * */

@Slf4j
public final class PersonDaoWriter implements IPersonDaoWriter {

    private static final int MAX_NUMBER_OF_PEOPLE_BEFORE_FLUSH = 100;

    private final PersonRepository repository;

    public PersonDaoWriter(PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public void savePerson(PersonEntity person) {
        repository.save(person);
    }

    @Override
    public void removePerson(PersonEntity person) {
        repository.delete(person);
    }

    @Override
    public Optional<PersonEntity> getPerson(Integer id) {
        return repository.findById(id);
    }

    @Override
    public void removePeople(List<PersonEntity> people) {
        repository.deleteAll(people);
    }
}
