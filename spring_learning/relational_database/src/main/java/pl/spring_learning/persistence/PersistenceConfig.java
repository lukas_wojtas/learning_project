package pl.spring_learning.persistence;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import pl.spring_learning.core.dao_abstraction.IPersonDaoReader;
import pl.spring_learning.core.dao_abstraction.IPersonDaoWriter;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.core.dao_abstraction.IProductDaoWriter;
import pl.spring_learning.entity.PersonEntity;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.persistence.dao.person.PersonDaoReader;
import pl.spring_learning.persistence.dao.person.PersonDaoWriter;
import pl.spring_learning.persistence.repository.PersonRepository;
import pl.spring_learning.persistence.dao.product.ProductDaoReader;
import pl.spring_learning.persistence.dao.product.ProductDaoWriter;
import pl.spring_learning.persistence.repository.ProductRepository;
import pl.spring_learning.persistence.properties.PersistenceProperties;

import javax.sql.DataSource;

/**
 * @Author Lukasz Wojtas
 * */
@Configuration
@ComponentScan
@EnableJpaRepositories
public class PersistenceConfig {

    @Bean
    IPersonDaoReader personReaderDao(PersonRepository repository) {
        return new PersonDaoReader(repository);
    }

    @Bean
    IPersonDaoWriter personWriterDao(PersonRepository repository) {
        return new PersonDaoWriter(repository);
    }

    @Bean
    @Primary
    IProductDaoReader productDaoReader(ProductRepository repository) {
        return new ProductDaoReader(repository);
    }

    @Bean
    IProductDaoWriter productDaoWriter(ProductRepository repository) {
        return new ProductDaoWriter(repository);
    }

    @Bean
    SessionFactory entityManagerFactory(DataSource dataSource
    ) {
        return new LocalSessionFactoryBuilder(dataSource)
                .addAnnotatedClasses(ProductEntity.class)
                .addAnnotatedClasses(PersonEntity.class)
                .buildSessionFactory();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

    @Bean
    DataSource getSqlDataSource(PersistenceProperties props) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(props.getUrl());
        config.setUsername(props.getUser());
        config.setPassword(props.getPassword());
        config.setDriverClassName(props.getDriverClassName());
        config.setConnectionTestQuery(props.getConnectionTestQuery());
        config.setConnectionTimeout(props.getConnectionTimeout());
        config.setMaximumPoolSize(props.getMaximumPoolSize());
        config.setMinimumIdle(props.getMinimumIdle());
        config.setInitializationFailTimeout(props.getInitializationFailTimeout());
        config.setPoolName(props.getPoolName());
        return new HikariDataSource(config);
    }
}
