package pl.spring_learning.definitions;

/**
 * Lukasz Wojtas
 * */
public final class KafkaTopics {
    public static final String CREATE_PRODUCT = "CREATE_PRODUCT";
    public static final String DELETE_PRODUCT = "DELETE_PRODUCT";
    public static final String PRODUCT_RESULT = "PRODUCT_RESULT";
}
