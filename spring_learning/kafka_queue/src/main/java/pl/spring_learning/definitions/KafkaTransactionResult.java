package pl.spring_learning.definitions;

/**
 * @Author Lukasz Wojtas
 * */
public enum KafkaTransactionResult {
    SUCCESS, FAILED, IN_PROGRESS
}
