package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;

/**
 * @Author Lukasz Wojtas
 * */
@Configuration
public class DataSourceServiceConfiguration {

    @Bean
    ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    DataValidator getValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }
}
