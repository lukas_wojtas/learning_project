package pl.spring_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class DataSourceService {

    public static void main(String[] args) {
        SpringApplication.run(DataSourceService.class, args);
    }
}
