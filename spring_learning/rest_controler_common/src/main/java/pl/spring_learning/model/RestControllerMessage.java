package pl.spring_learning.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Lukasz Wojtas
 */
@Getter
@AllArgsConstructor
public class RestControllerMessage {
    @Schema(description = "message from REST api, usually describing error")
    private final String message;
}
