package pl.spring_learning.service;

import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.ProductEntity;

/**
 * @Author Lukasz Wojtas
 * */

public interface IProductServiceConnector {
    ProductEntity getProduct(String name) throws RestControllerException;
}
