package pl.spring_learning.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.model.Basket;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.service.IBasketService;

/**
 * @Author Lukasz Wojtas
 */

@RestController
@Tag(name = "Basket", description = "Basket API")
public class BasketRestController {

    public static final String EMPTY_BASKET = "Cart is empty";

    @Autowired
    private IBasketService basket;

    @Operation(summary = "Get basket", tags = {"basket"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = Basket.class)))
    @ApiResponse(responseCode = "404", description = "Product not found in stock",
            content = @Content(schema = @Schema(implementation = RestControllerMessage.class)))
    @RequestMapping(value = "/supermarket/basket", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public Basket basket() {
        return basket.getBasket();
    }

    @Operation(summary = "Add Product to Basket", tags = {"basket"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = Basket.class)))
    @RequestMapping(value = "/supermarket/basket/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public Basket addProduct(
            @Parameter(description = "Name of the product", required = true)
            @RequestParam(name = "name") String productName,
            @Parameter(description = "Amount of products added to basket")
            @RequestParam(name = "amount", required = false, defaultValue = "1") int amount) throws RestControllerException {
        basket.addProduct(productName, amount);
        return basket.getBasket();
    }

    @Operation(summary = "Remove Product from Basket", tags = {"basket"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = Basket.class)))
    @RequestMapping(value = "/supermarket/basket/remove", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public Basket removeProduct(
            @Parameter(description = "Name of the product", required = true)
            @RequestParam(name = "name") String productName,
            @Parameter(description = "Amount of products removed basket")
            @RequestParam(name = "amount", required = false, defaultValue = "1") int amount) throws RestControllerException {
        basket.removeProduct(productName, amount);
        return basket.getBasket();
    }

    @Operation(summary = "Clear basket", tags = {"basket"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = Basket.class)))
    @RequestMapping(value = "/supermarket/basket/clear", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Basket clearBasket() {
        basket.clearBasket();
        return basket.getBasket();
    }

    @ExceptionHandler({RestControllerException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public RestControllerMessage handleControllerError(RestControllerException e) {
        return e.getRestControllerMessage();
    }
}
