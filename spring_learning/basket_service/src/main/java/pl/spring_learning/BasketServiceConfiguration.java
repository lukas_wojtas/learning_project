package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.spring_learning.properties.ProductServiceProperties;

import javax.net.ssl.*;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @Author Lukasz Wojtas
 */

@Configuration
@Slf4j
public class BasketServiceConfiguration {
    @Value("${server.ssl.trust-store-password}")
    private String trustStorePassword;
    @Value("${server.ssl.trust-store}")
    private Resource trustResource;
    @Value("${server.ssl.key-store-password}")
    private String keyStorePassword;
    @Value("${server.ssl.key-password}")
    private String keyPassword;
    @Value("${server.ssl.key-store}")
    private Resource keyStore;

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info().title("Product Reader Service API").description(
                        "This is description of REST interface which enable interaction with Product Reader Service"));
    }

    @Bean
    @Primary
    public OkHttpClient getHttpClient(ProductServiceProperties props) throws NoSuchAlgorithmException,
            KeyManagementException, CertificateException, KeyStoreException, IOException, UnrecoverableKeyException {
        X509Certificate cert = this.getCertificate();
        X509TrustManager[] trustManagers = new X509TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                        if (chain.length != 1) {
                            throw new CertificateException("Chain is wrong");
                        }
                        try {
                            cert.verify(chain[0].getPublicKey());
                        } catch (NoSuchAlgorithmException | InvalidKeyException |
                                NoSuchProviderException | SignatureException e) {
                            log.error("Certificate not verified with: " + e.getMessage());
                        }
                        chain[0].checkValidity();
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[]{};
                    }
                }
        };
        SSLContext context = getSslContext();
        return new OkHttpClient.Builder()
                .sslSocketFactory(context.getSocketFactory(), trustManagers[0])
                .hostnameVerifier((hostname, session) -> true)
                .authenticator((route, response) -> {
                    String credential = Credentials.basic(props.getUser(), props.getPassword());
                    return response.request().newBuilder().header("Authorization", credential).build();
                }).build();
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    private X509Certificate getCertificate() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        ClassPathResource resource = new ClassPathResource("basket-service.p12");
        InputStream inputStream = resource.getInputStream();
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(inputStream, "password".toCharArray());
        return (X509Certificate) ks.getCertificate("basket-service");
    }

    private SSLContext getSslContext()
            throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException,
            UnrecoverableKeyException, KeyManagementException {
        return SSLContexts.custom().loadTrustMaterial(
                trustResource.getURL(),
                trustStorePassword.toCharArray())
                .loadKeyMaterial(
                        keyStore.getURL(),
                        keyStorePassword.toCharArray(),
                        keyPassword.toCharArray()).build();
    }
}
