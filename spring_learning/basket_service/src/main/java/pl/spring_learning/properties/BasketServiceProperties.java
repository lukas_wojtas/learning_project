package pl.spring_learning.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * @Author Lukasz Wojtas
 * */
@ConfigurationProperties(prefix = "basket")
@Data
@Configuration
public class BasketServiceProperties {
    private Integer port;
    private String sslPassword;
}
