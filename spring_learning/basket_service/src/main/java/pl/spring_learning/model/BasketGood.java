package pl.spring_learning.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author Lukasz Wojtas
 */

@Getter
@AllArgsConstructor
public class BasketGood {

    @Schema(description = "quantity of given good in Basket")
    private final int quantity;

    @Schema(description = "Overall price for given product")
    private final int priceForProduct;

    @Schema(description = "Product Name")
    private final String name;
}
