package pl.spring_learning.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.spring_learning.ServiceSecurityConfiguration;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.controller.BasketRestController;
import pl.spring_learning.model.Basket;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.utils.BasketGenerator;
import pl.spring_learning.utils.TestContextBeanConfiguration;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static pl.spring_learning.utils.ProductsPojoFactory.BREAD;
import static pl.spring_learning.utils.ProductsPojoFactory.BUTTER;
import static pl.spring_learning.utils.ProductsPojoFactory.COLA;
import static pl.spring_learning.utils.ProductsPojoFactory.MOBILE;
import static pl.spring_learning.utils.ProductsPojoFactory.SKI;
import static pl.spring_learning.utils.ProductsPojoFactory.SNOWBOARD;
import static pl.spring_learning.service.ProductServiceConnector.NO_CONNECTION_TO_HOST;
import static pl.spring_learning.service.ProductServiceConnector.PRODUCT_NOT_FOUND;

/**
 * @Author Lukasz Wojtas
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {
        TestContextBeanConfiguration.class,
        BasketRestController.class,
        ServiceSecurityConfiguration.class
})

public class BasketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BasketRestController basketRestController;

    private final ObjectMapper mapper = new ObjectMapper();

    private final Basket emptyBasket = new Basket(new ArrayList<>(), 0);

    @Before
    @WithMockUser(value = "USER")
    public void setUp() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/clear")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(emptyBasket)));
    }

    @Test
    @WithMockUser(value = "USER")
    public void testGetEmptyBasket() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/supermarket/basket")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(emptyBasket)));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testAddingAndRemovingFromBasket() throws Exception {
        BasketGenerator basketGenerator = new BasketGenerator();
        reset(TestContextBeanConfiguration.getProductServiceConnector());
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=bread")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(basketGenerator.addProduct(BREAD.getProduct(), 1)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=bread")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(emptyBasket)));
        basketGenerator.removeProduct(BREAD.getProduct(), 1);
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(2)).getProduct(BREAD.getName());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=bread")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(basketGenerator.addProduct(BREAD.getProduct(), 1)));
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(4)).getProduct(BREAD.getName());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testAddManyProductsToBasket() throws Exception {
        BasketGenerator basketJSONUtil = new BasketGenerator();
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(SKI.getName())).thenReturn(SKI.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=ski&amount=4")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(basketJSONUtil.addProduct(SKI.getProduct(), 4)));
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=cola&amount=7")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(basketJSONUtil.addProduct(COLA.getProduct(), 7)));
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=bread&amount=30")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
                .json(basketJSONUtil.addProduct(BREAD.getProduct(), 30)));
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=bread")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
                .json(basketJSONUtil.addProduct(BREAD.getProduct(), 1)));
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(BUTTER.getName())).thenReturn(BUTTER.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=butter").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
                .json(basketJSONUtil.addProduct(BUTTER.getProduct(), 1)));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testRemoveManyProductFromBasket() throws Exception {
        BasketGenerator util = new BasketGenerator();
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(SNOWBOARD.getName())).thenReturn(SNOWBOARD.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=snowboard&amount=2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
                .json(util.addProduct(SNOWBOARD.getProduct(), 2)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=snowboard")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.removeProduct(SNOWBOARD.getProduct(), 1)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=snowboard")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
                .json(mapper.writeValueAsString(emptyBasket)));
        util.removeProduct(SNOWBOARD.getProduct(), 1);
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BUTTER.getName())).thenReturn(BUTTER.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=butter&amount=13")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.addProduct(BUTTER.getProduct(), 13)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=butter&amount=5")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.removeProduct(BUTTER.getProduct(), 5)));
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(MOBILE.getName())).thenReturn(MOBILE.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=mobile&amount=3")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.addProduct(MOBILE.getProduct(), 3)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=mobile&amount=2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.removeProduct(MOBILE.getProduct(), 2)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=mobile&amount=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.removeProduct(MOBILE.getProduct(), 1)));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testAddProductThatDoesNotExists() throws Exception {
        final String FAKE = "fake";
        reset(TestContextBeanConfiguration.getProductServiceConnector());
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(FAKE))
                .thenThrow(new RestControllerException(PRODUCT_NOT_FOUND));
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=" + FAKE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(new RestControllerMessage(PRODUCT_NOT_FOUND))));
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(1)).getProduct(FAKE);
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        BasketGenerator util = new BasketGenerator();
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=cola&amount=6").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.addProduct(COLA.getProduct(), 6)));
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(2)).getProduct(COLA.getName());
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(1)).getProduct(FAKE);
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=" + FAKE).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(new RestControllerMessage(PRODUCT_NOT_FOUND))));
        mockMvc.perform(MockMvcRequestBuilders.get("/supermarket/basket")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(util.toString()));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testRemoveProductThatDoesNotExists() throws Exception {
        final String FAKE = "fake";
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=" + FAKE).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(emptyBasket)));
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        BasketGenerator generator = new BasketGenerator();
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=cola&amount=6").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(generator.addProduct(COLA.getProduct(), 6)));
        mockMvc.perform(MockMvcRequestBuilders.delete("/supermarket/basket/remove?name=" + FAKE).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(generator.toString()));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testTotalPriceForBasket() throws Exception {
        BasketGenerator util = new BasketGenerator();
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        util.addProduct(COLA.getProduct(), 7);
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=cola&amount=7")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(util.toString()));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testNoStorageFound() throws Exception {
        when(TestContextBeanConfiguration
                .getProductServiceConnector()
                .getProduct(SNOWBOARD.getName()))
                .thenThrow(new RestControllerException(NO_CONNECTION_TO_HOST));
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/supermarket/basket/add?name=snowboard&amount=6")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(new RestControllerMessage(NO_CONNECTION_TO_HOST))));
    }
}
