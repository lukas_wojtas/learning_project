/*
 * @author Lukasz Wojtas
 */

package pl.spring_learning.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.service.BasketService;

public class BasketGenerator {

    private final BasketService basketService = new BasketService(TestContextBeanConfiguration.getProductServiceConnector());

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String toString() {
        try {
            return mapper.writeValueAsString(basketService.getBasket());
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public String addProduct(ProductEntity product, int quantity) throws RestControllerException {
        try {
            basketService.addProduct(product.getName(), quantity);
            return mapper.writeValueAsString(basketService.getBasket());
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public String removeProduct(ProductEntity product, int quantity) {
        basketService.removeProduct(product.getName(), quantity);
        try {
            return mapper.writeValueAsString(basketService.getBasket());
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
