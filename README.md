# Scalable internet shop application
Author: Lukasz Wojtas

### Version

* Version:1.0.1
markdown-header-product-writer-service-architecture
## Table of contents
1. [Perquisites](#markdown-header-perquisites)
2. [High Level Description](#markdown-header-high-level-description)
3. [Product Reader Service Architecture](#markdown-header-product-reader-service-architecture)
4. [Basket Service Architecture](#markdown-header-basket-service-architecture)
5. [Product Writer Service Architecture](#markdown-header-product-writer-service-architecture)
6. [Data Source Service Architecture](#markdown-header-data-source-service-architecture)
7. [Environment configuration](#markdown-header-environment-configuration)
8. [REST api description](#markdown-header-rest-api-description)
9. [Security](#markdown-security)

### Perquisites

1. **Docker** need to be installed on developer machine.
2. **Gradle** is used to build project.

### High Level Description
Project is Microservice based attempt to implement scalable
back-end part of an online shop. Overall Architecture is shown below.

![Alt text](UML/pics/overall_architecture.png?raw=true "Supermarket overall architecture")

[Fig.1] Overall architecture of the solution.

The solution consist of few layers of services.
1. Basket Service - provide basket like functionality for a shop. It is deployed as docker.
2. Product Reader Service - provide access to products for user and all other services.
It is deployed as docker.
3. Product Writer Service - provide a way to add and delete products.
It is deployed as docker.
4. Data Source Service - part of the persistence layer, handle all transactions to database.
It is deployed as docker.
5. Database - Preconfigured MySql 8.0 deployed as docker.
6. Kafka Queue - Use by persistence layer to route database operations, as well as producing 
notifications about transaction results for other services to receive. It is deployed as docker.

### Product Reader Service Architecture

**Product Reader Service** is responsible for providing access to products.
It exposes products in form of REST interface detailed below.
It is **read only** service and as such can't execute operations which change database state. 
Service is design to lower the number of calls to database as much as possible.
This is achieved by caching products internally as well as processing notifications received from Kafka Queue.
Overall flow of **Product Reader Service** is shown below:

![Alt text](UML/pics/product_reader.png?raw=true "Product Reader Service")

[Fig.2] Product Reader Service sequence diagram

### Basket Service Architecture

**Basket Service** is responsible for providing User with functionality of online shop basket.
When user add or remove products from the basket, **Basket Service** calculate overall price of all products currently 
in the basket with regard to special offers. **Basket Service** uses REST API of **Product Reader Service** to retrieve 
product information. Products can be added or removed with quantity from Basket.
Overall flow of **Basket Service** is shown below:

![Alt text](UML/pics/basket_service.png?raw=true "Basket Service")

[Fig. 3] Basket Service Service sequence diagram

### Product Writer Service Architecture ###
**Product Writer Service** is responsible for adding and removing products from stock. **Product Writer Service** expose 
REST api to enable user to change database state. **Product Writer Service** do NOT access database directly instead using 
Kafka queue to send transactions to **Data Source Service** and receive notifications about results of those transactions.
**Product Writer Service** implements **REST talk** pattern to enable user to monitor progress of the transactions.
After configurable time period, **Product Writer Service** clears transaction data automatically.
Overall flow of **Product Writer Service** is shown below:

![Alt text](UML/pics/product_writer.png?raw=true "Product Writer Service")

[Fig. 4] Product Writer Service sequence diagram

### Data Source Service Architecture
**Data Source Service** responsibility is to perform CREATE, DELETE operations on Database. It uses Kafka queue to receive
requested transactions. When received, **Data Source Service** perform operation on Database and produces message to Kafka
queue. This message contains result of given transaction.
Overall flow of **Data Source Service** is shown below:

![Alt text](UML/pics/data_source_service.png?raw=true "Data Source Service")

[Fig.5] Data Source Service Sequence diagram

### Environment configuration

1. Create properties files from templates.
    * Create **application.yml** file from the template located in **spring_learning/relational_database/src/test/resources**.
    Please replace **docker_ip_address** with ip address of docker and **username** and **password** with credentials to database.
    * Create **application.yml** file from the template located in **spring_learning/basket_service/src/main/resources**.
    Please replace **docker_ip_address** with ip address of docker.
    * Create **application.yml** file from the template located in **spring_learning/kafka_queue/src/main/resources**.
    Please replace **docker_ip_address** with ip address of docker.
    * Create **application.yml** file from the template located in **spring_learning/product_reader/src/main/resources**.
    Please replace **docker_ip_address** with ip address of the docker and **username** and **password** with credentials to database.
    Please replace **docker_ip_address** with ip address of the docker for **kafka bootstrap** property.
    * Create **application.yml** file from the template located in **spring_learning/product_writer/src/main/resources**.
    Please replace **docker_ip_address** with ip address of the docker.
    Please replace **docker_ip_address** with ip address of the docker for kafka bootstrap property.
    * Create **application.yml** file from the template located in **spring_learning/data_source_service/src/main/resources**.
    Please replace **docker_ip_address** with ip address of the docker and **username** and **password** with credentials to database.
    Please replace **docker_ip_address** with ip address of the docker for **kafka bootstrap** property.
2. Build MySql docker image and deploy on docker
    * Create **application.yml** file from the template located in **spring_learning/mysql_docker/src/test/resources**.
    Please replace **username** and **password** with credentials to database.
    * Please navigate to spring_learning directory. Next please execute following command to build docker image:
    
            docker build -t myshop-mysql:v1 -f ./mysql_docker/Dockerfile .
    
    * When docker is build with success, please run the image with following command where **username** and **password** are database credentials:
    
            docker run -p 3306:3306 --env "MYSQL_DATABASE=test" --env "MYSQL_PASSWORD=password" --env "MYSQL_USER=username" --env "MYSQL_ROOT_PASSWORD=admin"  -d --name=myshop-mysql myshop-mysql:v1
         
    * Verify that container is up and running with following command:
    
            docker ps
    
3. Deploy kafka on docker.

    * Create docker network for kafka with following command:
    
            docker network create kafka-net --driver bridge
            
    * Run zookeeper with following command:
    
            docker run --name zookeeper-server -d -p 2181:2181 --network kafka-net -e ALLOW_ANONYMOUS_LOGIN=yes bitnami/zookeeper:latest
            
    * Replace docker_ip_address with address set in kafka-server.properties file. When replaced, run Kafka broker with following command:
    
            docker run --name kafka-server1 -d --network kafka-net -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper-server:2181 -e KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://docker_ip_address:9092 -p 9092:9092 bitnami/kafka:latest
4. To generate self signed certificates user need to run script located here:
**spring_learning/src/main/resources/generate_certs.sh**
Certificates should be automatically generated into resources folder of services below:

    * Basket Service
    * Product Reader
    * Product Writer.

5. Run all other services.
    * Using gradle task shown on Figure 6 run all other services
    
    ![Alt text](UML/pics/run_services.png?raw=true "Run Services with Gradle task")

**Please note that all above operations are possible to achieve by using gradle tasks in main project. 
To be able to use those tasks, user need to provide IDE with access to the docker terminal. 
For example on Ubuntu Linux this can be achieved with following command:**

``sudo chmod 666 /var/run/docker.sock``

### REST api description

Description of the REST api for **Basket Service** is available at address below. 
Please replace **docker_ip** with an actual docker ip address.
**Note!! Basket Service** need to be running.

``http://docker_ip:8012/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/``

Description of the REST api for **Product Reader Service** is available at address below. 
Please replace **docker_ip** with an actual docker ip address.
**Note!! Product Reader Service** need to be running.

``http://docker_ip:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/``

Description of the REST api for **Product Reader Service** is available at address below. 
Please replace **docker_ip** with an actual docker ip address.
**Note!! Product Writer Service** need to be running.

``http://docker_ip:8014/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#``

### Security

All **REST** interfaces are using **base authentication**. It is defined in yaml files and loaded at start of the 
service. Services secured with **base authentication** are:

* Basket Service
* Product Reader
* Product Writer.

**TSL** is introduced to all **REST** service with use of self signed certificates.
To run project certificates need to be generated by means of running delivered script.

**Mutual Authentication** was introduced between **Basket Service** and **Product Reader**.
In both **Product Reader** and **Basket Service** it is not mandatory and set to **want**. 
It is done because **POSTMAN** do not really support self signed certs. User can set **Mutual Authentication** 
to **need** in **Product Reader** and use **Basket Service** to add Product to basket. This will force **Basket Service**
to use **Mutual Authentication** and prove it works.

### Contact
* lukas.wojtas@gmail.com